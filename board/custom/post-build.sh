echo "Modifying root partition to mount read only"
sed -i 's/\/dev\/root.*/\/dev\/root       \/               ext4    ro,sync,noauto       0       1/' output/target/etc/fstab

echo "Overwrite inittab"
cp board/custom/inittab output/target/etc/inittab
rm -Rf output/target/etc/init.d

echo "Setting .profile"
cp board/custom/profile output/target/root/.profile

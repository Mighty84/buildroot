echo "Setting custom .config"
cp board/custom/config .config

echo "Generating SSH key pair for target"
ssh-keygen -t rsa -b 4096 -f output/images/qemu_rsa -N ""
cat output/images/qemu_rsa.pub > output/images/authorized_keys

echo "Registering private key to ~/.ssh/config"
IDF="IdentityFile $PWD/output/images/qemu_rsa"
grep -q "$IDF" ~/.ssh/config && \
	echo "Already registered" || \
	(echo $IDF >> ~/.ssh/config && echo "Registered")

make



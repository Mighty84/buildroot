# lpj= 
# quiet lowers verbosity of kernel output
QEMU_AUDIO_DRV=none qemu-system-arm -M vexpress-a9 \
	-kernel output/images/zImage \
	-dtb output/images/vexpress-v2p-ca9.dtb \
	-drive if=sd,format=raw,file=output/images/rootfs.ext2 \
	-append "console=ttyAMA0 root=/dev/mmcblk0 rootfstype=ext4 ro quiet lpj=3260416" \
	-nographic \
	-net nic \
	-net user,hostfwd=tcp::2222-:22 \
	-m 512 \
	-cpu cortex-a9 \
	-smp 4
#	-device usb-ehci,id=ehci

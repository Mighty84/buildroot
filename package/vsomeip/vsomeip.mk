################################################################################
#
# vsomeip
#
################################################################################

VSOMEIP_VERSION = 2.6.2
VSOMEIP_SOURCE = $(VSOMEIP_VERSION).tar.gz
VSOMEIP_SITE = https://github.com/GENIVI/vsomeip/archive
VSOMEIP_LICENSE = MPL-2.0
VSOMEIP_LICENSE_FILES = LICENSE
VSOMEIP_INSTALL_STAGING = YES

VSOMEIP_CONF_OPTS += \
	-DENABLE_DEFAULT=OFF 

VSOMEIP_DEPENDENCIES += boost

define VSOMEIP_INSTALL_INIT_SYSV
        $(INSTALL) -D -m 0755 $(VSOMEIP_PKGDIR)/S51vsomeipd \
                $(TARGET_DIR)/etc/init.d/S51vsomeipd
endef

$(eval $(cmake-package))


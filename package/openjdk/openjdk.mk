################################################################################
#
# OPENJDK
#
################################################################################

OPENJDK_VERSION = cfeea66a3fa8:a08cbfc0e4ec
OPENJDK_SITE = http://hg.openjdk.java.net/jdk9/jdk9
OPENJDK_SITE_METHOD=hg
OPENJDK_LICENSE = GPL-2.0
OPENJDK_LICENSE_FILES = LICENSE
OPENJDK_AUTORECONF = NO

define OPENJDK_GET_SOURCE
	mkdir -p $(@D)/.hg && cp $(OPENJDK_PKGDIR)/hgrc $(@D)/.hg  && \
	cd $(@D) && \
	chmod 755 get_source.sh && ./get_source.sh
endef
#OPENJDK_PRE_CONFIGURE_HOOKS += OPENJDK_GET_SOURCE
OPENJDK_POST_EXTRACT_HOOKS += OPENJDK_GET_SOURCE

OPENJDK_CONF_OPTS = \
	--with-jdk-variant=normal \
	--with-jvm-variants=minimal \
	--with-debug-level=release \
	--disable-debug-symbols \
	--disable-zip-debug-info \
	--enable-unlimited-crypto \
	--enable-unlimited-crypto \
	--with-zlib=system \
	--openjdk-target="arm-linux" \
	--with-sys-root=$(STAGING_DIR) \
	-disable-warnings-as-errors
	--with-extra-cflags="$_CFLAGS -w" \
	--with-extra-cxxflags="-w" \
	--with-stdc++lib=dynamic \
	--without-x \
	--without-cups \
	--without-alsa \
	--disable-freetype-bundling \
	--disable-hotspot-gtest \
	--enable-headless-only
# TODO
#	--openjdk-target="$(GNU_TARGET_NAME)" \
#	--with-libpng=system \

define OPENJDK_CONFIGURE_CMDS
	cd $(@D) && chmod 755 configure && \
	$(TARGET_CONFIGURE_OPTS) ./configure $(OPENJDK_CONF_OPTS) OBJCOPY=$(TARGET_OBJCOPY) STRIP=$(TARGET_STRIP) CPP_FLAGS=-lstdc++ CXX_FLAGS=-lstdc++ CPP=$(TARGET_CPP) CXX=$(TARGET_CXX) CC=$(TARGET_CC) LD=$(TARGET_CC)
endef

define OPENJDK_BUILD_CMDS
# TODO
#        $(TARGET_MAKE_ENV) $(MAKE) -C $(@D)
        $(TARGET_MAKE_ENV) make jre-image -C $(@D)
endef

SRC = build/linux-arm-normal-minimal-release

define OPENJDK_INSTALL_TARGET_CMDS
#        mkdir -p $(TARGET_DIR)/usr/lib/jli
#        mkdir -p $(TARGET_DIR)/usr/modules
# TODO
# Exclude debuginfo and diz
#        cp -aLf $(@D)/$(SRC)/jdk/bin/* $(TARGET_DIR)/usr/bin
#        cp -aLf $(@D)/$(SRC)/jdk/lib/*.so $(TARGET_DIR)/usr/lib
#        cp -aLf $(@D)/$(SRC)/jdk/lib/jli/*.so $(TARGET_DIR)/usr/lib/jli
#        cp -aLf $(@D)/$(SRC)/jdk/lib/minimal/*.so $(TARGET_DIR)/usr/lib/minimal
# Copy only relevant
#        cp -aLf $(@D)/$(SRC)/jdk/modules/* $(TARGET_DIR)/usr/modules
#	cp -af $(@D)/$(SRC)/support/modules_libs/java.base/jvm.cfg $(TARGET_DIR)/usr/lib

	mkdir -p $(TARGET_DIR)/usr/lib/jvm
	cp -aLrf $(@D)/$(SRC)/images/jre/* $(TARGET_DIR)/usr/lib/jvm
endef

$(eval $(generic-package))
